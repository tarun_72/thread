package com.thread.cyclicbarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierAPP {

	public static void main(String[] args) {
		CyclicBarrier barrier = new CyclicBarrier(3, new Barrier());
		JobCycle cycle = new JobCycle(barrier);
		JobCycle cycle1 = new JobCycle(barrier);
		JobCycle cycle2 = new JobCycle(barrier);
		cycle.start();
		cycle1.start();
		cycle2.start();


		

	}
}

class Barrier extends Thread {

	@Override
	public void run() {
		super.run();
		System.out.println("Master task completed ");

	}

}

class JobCycle extends Thread {
	CyclicBarrier cyclicBarrier;

	public JobCycle(CyclicBarrier cyclicBarrier) {
		this.cyclicBarrier = cyclicBarrier;
	}

	@Override
	public void run() {
		super.run();
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
				System.out.println("Time is"+i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			cyclicBarrier.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("task completed " + getName());

	}
}
