package com.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class ExampleThreadYeld {

	static AtomicInteger atomicInteger = new AtomicInteger(0);
	public static void main(String[] args) {
		WorldPrintClass class1 = new WorldPrintClass(atomicInteger);
		class1.setPriority(5);
		class1.start();

		while(atomicInteger.intValue()<15) {
		     System.out.println("World");
	         atomicInteger.incrementAndGet();
			Thread.yield();
			try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Thread.currentThread().setPriority(5);
		
		
	}

}

class WorldPrintClass extends Thread{
	AtomicInteger atomicInteger;

	public WorldPrintClass(AtomicInteger atomicInteger) {
		this.atomicInteger = atomicInteger;
		
	}

	
	@Override
	public void run() {
		super.run();
		while(atomicInteger.intValue()<15) {
			try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.print("Hello");
			Thread.yield();
		}
	}
}
