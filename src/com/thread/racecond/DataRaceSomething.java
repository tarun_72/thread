package com.thread.racecond;

public class DataRaceSomething {
	static String theory = "Race condition: A race condition is a situation, "
			+ "in which the result of an operation depends on the interleaving "
			+ "of certain individual operations.\r\n." + "Data race: A data race is a situation,"
			+ "in which at least two threads access a shared variable at the same time. At least on thread tries to modify the variable.\r\n"
			+ "";

	public static void main(String[] args) {
		//System.out.println("******* Race Conditions and Data Thread ******");
		//System.out.println("" + theory);
		//System.out.println("******Example Simple Executions*****");
		CounterR counter = new CounterR();
		for (int i = 0; i < 1000; i++) {
			counter.incrementCounter();
		}
		for (int i = 0; i < 1000; i++) {
			counter.decrementCounter();
		}
		//System.out.println(counter.counterValue());
		
		//System.out.println("******Example IN Thread Enviroment*****");
		
		ThreadIncr increment = new ThreadIncr(counter);

		ThreadDecr decrement = new ThreadDecr(counter);
		increment.start();

		decrement.start();

		try {

			increment.join();
			decrement.join();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(counter.counterValue());

		//System.out.println("******End of Main Method*****");

	}

}

class CounterR {
	private int counter = 0;

	void incrementCounter() {
		counter++;

	}

	void decrementCounter() {
		counter--;

	}

	int counterValue() {
		return counter;
	}

}

class ThreadIncr extends Thread {
	private CounterR counter; // shared variables

	public ThreadIncr(CounterR counter) {
		super();
		this.counter = counter;
	}

	@Override
	public void run() {
		super.run();

		for (int i = 0; i < 1000; i++) {
			counter.incrementCounter();
		}
	}

}

class ThreadDecr extends Thread {
	CounterR counter; // shared variables

	public ThreadDecr(CounterR counter) {
		super();
		this.counter = counter;
	}

	@Override
	public void run() {
		super.run();
		for (int i = 0; i < 1000; i++) {
			counter.decrementCounter();
		}
	}

}
