package com.thread.racecond;

public class DataRace {
	static String theory = "Race condition: A race condition is a situation, "
			+ "in which the result of an operation depends on the interleaving "
			+ "of certain individual operations.\r\n." + "Data race: A data race is a situation,"
			+ "in which at least two threads access a shared variable at the same time. At least on thread tries to modify the variable.\r\n"
			+ "";

	public static void main(String[] args) {
		System.out.println("******* Race Conditions and Data Thread ******");
		System.out.println("" + theory);
		System.out.println("******Example Simple Executions*****");
		Counter counter = new Counter();
		counter.incrementCounter();
		counter.decrementCounter();
		System.out.println(counter.counterValue());
		System.out.println("******Example IN Thread Enviroment*****");
		ThreadIncrement increment = new ThreadIncrement(counter);
		increment.start();

		ThreadDecrement decrement = new ThreadDecrement(counter);
		decrement.start();
		
		
		try {

			increment.join();

			decrement.join();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(counter.counterValue());

		System.out.println("******End of Main Method*****");

	}

}

class Counter {
	private int counter = 0;

	void incrementCounter() {
		for (int i = 0; i < 1000; i++) {
			counter++;
		}
		System.out.println("value of counter increment "+counter);
	}

	void decrementCounter() {
		for (int i = 0; i < 1000; i++) {
			counter--;
		}
		System.out.println("value of counter decrement "+counter);

	}

	int counterValue() {
		return counter;
	}

}

class ThreadIncrement extends Thread {
	private Counter counter; // shared variables

	public ThreadIncrement(Counter counter) {
		super();
		this.counter = counter;
	}

	@Override
	public void run() {
		super.run();
		try {
			Thread.sleep(59);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		counter.incrementCounter();
	}

}

class ThreadDecrement extends Thread {
	Counter counter; // shared variables

	public ThreadDecrement(Counter counter) {
		super();
		this.counter = counter;
	}

	@Override
	public void run() {
		super.run();
		counter.decrementCounter();
	}

}
