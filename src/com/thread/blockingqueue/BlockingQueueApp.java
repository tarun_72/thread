package com.thread.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueApp {
	public static void main(String[] args) {
		BlockingQueue<Integer>blockingQueue  = new ArrayBlockingQueue<Integer>(10);
		ProducerTask producerTask = new ProducerTask(blockingQueue);
		ConsumerTask consumerTask = new ConsumerTask(blockingQueue);
		producerTask.start();
		consumerTask.start();

	}

}

class ProducerTask extends Thread {
	BlockingQueue<Integer> block;
	int counter = 0;

	public ProducerTask(BlockingQueue<Integer> block) {
		super();
		this.block = block;
	}

	@Override
	public void run() {
		super.run();
		try {
			while (true) {
				System.out.println("producing more products"+counter);
				block.put(counter++);
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class ConsumerTask extends Thread {
	BlockingQueue<Integer> block;
	int counter = 0;

	public ConsumerTask(BlockingQueue<Integer> block) {
		super();
		this.block = block;
	}

	@Override
	public void run() {
		super.run();
		try {
			while (true) {
				int number = block.take();
				System.out.println("Consumer Consuming products" + number);
				Thread.sleep(1500);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
