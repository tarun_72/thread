package com.thread.deadlock;

public class DeadLockExample {
	static Resource resource = new Resource();
	static Resource resource2 = new Resource();

	private static void connectResource() {
		synchronized (resource) {
			System.out.println("resource1 accuried by thread" + Thread.currentThread());
			synchronized (resource2) {
				System.out.println("resource2 accuried by thread" + Thread.currentThread());

			}
		}
	}

	private static void connectResource2() {
		synchronized (resource2) {
			System.out.println("resource2 accuried by thread" + Thread.currentThread());
			synchronized (resource) {
				System.out.println("resource1 accuried by thread" + Thread.currentThread());

			}
		}
	}

	public static void main(String[] args) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				connectResource();			

			}
		});

		Thread thread2 = new Thread(new Runnable() {

			@Override
			public void run() {
				connectResource2();			

			}
		});
		thread.start();
		thread2.start();
	}

}

class Resource {
	public Resource() {
		System.out.println("resources created");
	}

}
