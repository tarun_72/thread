package com.thread.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreExampleApp {

	public static void main(String[] args) {
		System.out.println("*******************Semaphore********************");
		System.out.println("");
		System.out.println("*******************Semaphore End********************");

		ExecutorService executorService = Executors.newCachedThreadPool();
		for (int i = 0; i < 200; i++) {
			executorService.execute(new Runnable() {

				@Override
				public void run() {
						Downloader.Instance.Downloader();
				}
			});
		}

	}

}

enum Downloader {
	Instance;

	private Semaphore semaphore = new Semaphore(3, true);

	 void Downloader() {

		try {
			semaphore.acquire();
			downloadSimulation();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			semaphore.release();
		}

	}

	void downloadSimulation() {
		System.out.println("downloading Data " +Thread.currentThread());

		try {
			Thread.sleep(2000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
