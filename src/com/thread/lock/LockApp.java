package com.thread.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockApp {
	static String theory = "A java.util.concurrent.locks."
			+ "Lock is a thread synchronization mechanism just like synchronized blocks."
			+ "A Lock is, however, more flexible and more sophisticated than a synchronized block. Since Lock is an interface, you need to use one of its implementations to use a Lock in your applications. "
			+ "ReentrantLock is one such implementation of Lock interface.";

	public static void main(String[] args) {
		System.out.println("************Lock Inteface in Java***********");
		System.out.println("" + theory);
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				increment();
			}
		});

		Thread thread2 = new Thread(new Runnable() {

			@Override
			public void run() {
				increment();

			}
		});
		thread.start();
		thread2.start();
		try {
			thread.join();
			thread2.join();
			System.out.println("counter =>" + Counter);
			System.out.println("Atomic counter =>" + AtomicCounter);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static Lock lock = new ReentrantLock();
	private Condition condition = lock.newCondition();
	private static int Counter = 0;
	private static int AtomicCounter = 0;

	private static void increment() {
		for (int i = 0; i < 1000; i++) {
			Counter++;
			lock.lock();
			AtomicCounter++;
			lock.unlock();
		}

	}
}
