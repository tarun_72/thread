package com.thread.join;

public class ThreadJoin {
	static String theory = "java.lang.Thread class provides the join() method "
			+ "which allows one thread to wait until another thread completes its execution. "
			+ "If t is a Thread object whose thread is currently executing,"
			+ "then t.join() will make sure that t is terminated before the next instruction is executed by the program.\r\n";

	public static void main(String[] args) {
		System.out.println("******* JOIN Method in thread ******");
		System.out.println("" + theory);
		System.out.println("******Example*****");

		ThreadTypeOne threadTypeOne = new ThreadTypeOne();
		ThreadTypeTwo threadTypeTwo = new ThreadTypeTwo();
		threadTypeOne.start();
		threadTypeTwo.start();

		try {
			threadTypeOne.join();
			threadTypeTwo.join();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("******* Main Thread Ends ******");

	}

}

class ThreadTypeOne extends Thread {

	@Override
	public synchronized void start() {
		super.start();
		System.out.println("thread stated" + getName());
	}

	@Override
	public void run() {
		super.run();
		System.out.println("In The Run ");
		for (int i = 0; i < 100; i++) {
			System.out.print(" j = " + i);
		}
		System.out.println(getName() + "  run method ends");

	}
}

class ThreadTypeTwo extends Thread {

	@Override
	public synchronized void start() {
		super.start();
		System.out.println("thread stated" + getName());
	}

	@Override
	public void run() {
		super.run();
		System.out.println("In The Run ");
		for (int i = 0; i < 100; i++) {
			System.out.print(" j = " + i);
		}
		System.out.println(getName() + "  run method ends");
	}
}
