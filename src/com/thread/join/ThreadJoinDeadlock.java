package com.thread.join;

public class ThreadJoinDeadlock {

	public static void main(String[] args) {
		

		System.out.println("******* DeadLock ******");
		System.out.println("******Example*****");
		ThreadBlocker.MainThread = Thread.currentThread();
		ThreadBlocker blocker = new ThreadBlocker();
		blocker.start();
		try {
			blocker.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("******Main method ends*****");

	}

}

class ThreadBlocker extends Thread {
	static Thread MainThread;

	@Override
	public synchronized void start() {
		super.start();
		System.out.println("thread stated" + getName());
	}

	@Override
	public void run() {
		super.run();
		System.out.println("In The Run ");
		for (int i = 0; i < 10; i++) {
			System.out.print(" j = " + i);
		}
		try {
			
			MainThread.join();
	System.out.println(getName() + "  Blocked ");

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(getName() + "  run method ends");

	}
}
