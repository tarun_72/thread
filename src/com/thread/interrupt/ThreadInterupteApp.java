package com.thread.interrupt;

public class ThreadInterupteApp {
	static String Theory = "An interrupt is an indication to a thread "
			+ "that it should stop what it is doing and do something else. "
			+ "It's up to the programmer to decide exactly how a thread responds to an interrupt,"
			+ "but it is very common for the thread to terminate";

	public static void main(String[] args) {
		System.out.println("******* Interupte Method in thread ******");
		System.out.println("" + Theory);
		System.out.println("******Example*****");
		
		TestInterruptingThread interruptingThread = new TestInterruptingThread();
		interruptingThread.start();
		try {
			Thread.sleep(20);
			System.out.println("******Sleep ends*****");

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		interruptingThread.interrupt();
		
		System.out.println("******Main method ends*****");


	}
	

}

class TestInterruptingThread extends Thread {
	@Override
	public synchronized void start() {
		super.start();
		System.out.println("thread stated " + getName());
	}

	@Override
	public void run() {
		super.run();
		System.out.println("In The Run ");
		try {
			sleep(30);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < 100; i++) {
			System.out.println("thread intrupted = "+isInterrupted());
			System.out.print(" j = " + i);
		}
		System.out.println(getName() + "  run method ends");

	}
}