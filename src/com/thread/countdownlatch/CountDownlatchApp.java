package com.thread.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class CountDownlatchApp {

	public static void main(String[] args) {
		CountDownLatch countDownLatch = new CountDownLatch(3);
		Job job = new Job(countDownLatch);
		Job job1 = new Job(countDownLatch);
		Job job2 = new Job(countDownLatch);
		job1.start();
		job2.start();
		job.start();
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("task completed ");
		
	}

}

class Job extends Thread {
	CountDownLatch countDownLatch;
	public Job(CountDownLatch countDownLatch) {
		this.countDownLatch = countDownLatch;
	}

	@Override
	public void run() {
		super.run();
		for(int i = 0;i<10;i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
			System.out.println("task completed "+getName());

			countDownLatch.countDown();
	
	}
}
