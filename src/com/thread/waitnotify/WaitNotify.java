package com.thread.waitnotify;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadPoolExecutor;


public class WaitNotify {
	static String theroyWaitAndNotify = "\r\n" + 
			" Wait \r\n" + 
			" The current thread must own this object's monitor.\r\n" + 
			" The thread releases ownership of this monitor and waits until another thread notifies threads waiting on this object's monitor to wake up either through a call to the notify method or the notifyAll method.\r\n" + 
			" The thread then waits until it can re-obtain ownership of the monitor and resumes execution.\r\n" + 
			" \r\n" + 
			" Notify \r\n" + 
			" For all threads waiting on this object's monitor (by using any one of the wait() method),\r\n" + 
			" the method notify() notifies any one of them to wake up arbitrarily.\r\n" + 
			" The choice of exactly which thread to wake is non-deterministic and depends upon the implementation.\r\n" + 
			"";
	static List<Integer> list = new ArrayList<Integer>();

	public static void main(String[] args) {
		System.out.println("************ Wait Notify ****************");
		System.out.println(""+theroyWaitAndNotify);
		
		Producer producer = new Producer(list);		
		Consumer consumer = new Consumer(list);
		producer.start();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		consumer.start();

	}

}


class Producer extends Thread {
	@Override
	public void run() {
		super.run();
		synchronized (list) {
			produce();

		}
	}

	List<Integer> list;

	public Producer(List<Integer> list) {
		super();
		this.list = list;
	}

	public synchronized void produce() {
		while (true) {
			while (list.size() == 3) {
				try {
					System.out.println("Queue is full ");

					list.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			int random = new Random().nextInt(1000);
			System.out.println("item added "+random);
			list.add(random);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			list.notify();
		}
	}
}

class Consumer extends Thread{
	List<Integer> list;
	@Override
	public void run() {
		super.run();
		synchronized (list) {
			consume();

		}
	}
	public Consumer(List<Integer> list) {
		super();
		this.list = list;
	}

	public synchronized void consume() {
		while (true) {
			while (list.size() == 0) {
				try {
					System.out.println("Queue is Empty ");

					list.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			int i = list.remove(list.size() - 1);
			System.out.println("item remove "+i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			list.notify();
		}
	}
}