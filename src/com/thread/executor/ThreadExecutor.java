package com.thread.executor;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadExecutor {

	public static void main(String[] args) {
		ArrayList<Task> arrayList = new ArrayList<Task>();
		for (int i = 0; i < 100; i++) {
			arrayList.add(new Task());
		}
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 100; i++) {
			executorService.submit(arrayList.get(i));
		}
		executorService.shutdown();
	}
}

class Task extends Thread {
	@Override
	public void run() {
		super.run();
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println( " count completed by " + Thread.currentThread().getName());

	}
}
