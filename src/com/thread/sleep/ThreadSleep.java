package com.thread.sleep;

public class ThreadSleep {

	public static void main(String[] args) {

		System.out.println("******* Sleep ******");
		System.out.println("******Example*****");
		ThreadSleeper sleeper = new ThreadSleeper();
		sleeper.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(sleeper.isAlive())
		sleeper.setSleep(2000);
		try {
			sleeper.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("******Main method ends*****");

	}

}

class ThreadSleeper extends Thread {

	@Override
	public synchronized void start() {
		super.start();
		System.out.println("thread stated" + getName());
	}

	@Override
	public void run() {
		super.run();
		System.out.println("In The Run ");
		for (int i = 0; i < 10; i++) {
			System.out.print(" j = " + i);
		}
	

			System.out.println(getName() + "  Blocked ");

		
		System.out.println(getName() + "  run method ends");

	}

	public void setSleep(long milli) {
		try {
			sleep(milli);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}