package com.thread;

public class ThreadYield {
	static String theory ="A yield() method is a static method of Thread class and "
			+ "it can stop the currently executing thread and "
			+ "will give a chance to other waiting threads of the same priority."
			+ " If in case there are no waiting threads or if all the waiting threads "
			+ "have low priority then the same thread will continue its execution.";

	public static void main(String[] args) {
		System.out.println("******* Yeild Method in thread ******");
		System.out.println(""+theory);
		System.out.println("******Example*****");
		ThreadType threadType = new ThreadType();
		threadType.start();
		threadType.yield();
		for(int i= 0;i<10;i++) {
			System.out.print(" i = "+i);
		}
		
	}
		
}
class ThreadType extends Thread{

	@Override
	public synchronized void start() {
		super.start();
		System.out.println("thread stated");
	}

	@Override
	public void run() {
		super.run();
		System.out.println("In The Run ");
		for(int i= 0;i<100;i++) {
			System.out.print(" j = "+i);
		}

	}
	
	
}